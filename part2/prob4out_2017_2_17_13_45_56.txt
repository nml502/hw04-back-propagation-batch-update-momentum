----------BP LEARN STARTED----------
***Initial weights***
	W1
	-0.092847 	-0.038674 	+0.011395 	+0.099870 	+0.010070 
	+0.094164 	-0.053419 	+0.059562 	-0.002418 	-0.094098 
	+0.006414 	+0.098359 	+0.050474 	-0.044166 	-0.082494 
	W2
	-0.092885 	+0.088521 	-0.083800 	-0.061784 
	-0.012918 	-0.019636 	-0.060924 	+0.020941 
	+0.003475 	+0.015178 	+0.023220 	+0.033099 
***Learning rate***
	0.01
***Momentum Update (forgetting factor)***
	0.5
***Stopping Criteria = max. learn steps OR mishit (percentage) train and test error < tolerance***
	Max learning steps
	225000
	Traiing Error Tolerance
	0%
	Test Error Tolerance
	4%
***Initial mishit Error for training ***
	66.666667%
***Initial mishit Error for test ***
	66.666667%
***Performance indicators while running BP algorithm***
	iter: 45, training mishit error:100.000000%, test mishit error:100.000000%
	iter: 90, training mishit error:98.666667%, test mishit error:100.000000%
	iter: 135, training mishit error:49.333333%, test mishit error:45.333333%
	iter: 180, training mishit error:42.666667%, test mishit error:38.666667%
	iter: 225, training mishit error:34.666667%, test mishit error:34.666667%
	iter: 270, training mishit error:36.000000%, test mishit error:34.666667%
	iter: 315, training mishit error:38.666667%, test mishit error:37.333333%
	iter: 360, training mishit error:33.333333%, test mishit error:33.333333%
	iter: 405, training mishit error:57.333333%, test mishit error:50.666667%
	iter: 450, training mishit error:42.666667%, test mishit error:41.333333%
	iter: 495, training mishit error:33.333333%, test mishit error:33.333333%
	iter: 540, training mishit error:42.666667%, test mishit error:38.666667%
	iter: 585, training mishit error:32.000000%, test mishit error:34.666667%
	iter: 630, training mishit error:30.666667%, test mishit error:33.333333%
	iter: 675, training mishit error:34.666667%, test mishit error:26.666667%
	iter: 720, training mishit error:25.333333%, test mishit error:24.000000%
	iter: 765, training mishit error:25.333333%, test mishit error:25.333333%
	iter: 810, training mishit error:28.000000%, test mishit error:26.666667%
	iter: 855, training mishit error:22.666667%, test mishit error:24.000000%
	iter: 900, training mishit error:22.666667%, test mishit error:20.000000%
	iter: 945, training mishit error:24.000000%, test mishit error:24.000000%
	iter: 990, training mishit error:21.333333%, test mishit error:24.000000%
	iter: 1035, training mishit error:20.000000%, test mishit error:21.333333%
	iter: 1080, training mishit error:21.333333%, test mishit error:18.666667%
	iter: 1125, training mishit error:18.666667%, test mishit error:16.000000%
	iter: 1170, training mishit error:16.000000%, test mishit error:21.333333%
	iter: 1215, training mishit error:18.666667%, test mishit error:14.666667%
	iter: 1260, training mishit error:8.000000%, test mishit error:12.000000%
	iter: 1305, training mishit error:13.333333%, test mishit error:12.000000%
	iter: 1350, training mishit error:6.666667%, test mishit error:10.666667%
	iter: 1395, training mishit error:5.333333%, test mishit error:9.333333%
	iter: 1440, training mishit error:5.333333%, test mishit error:8.000000%
	iter: 1485, training mishit error:5.333333%, test mishit error:8.000000%
	iter: 1530, training mishit error:10.666667%, test mishit error:9.333333%
	iter: 1575, training mishit error:8.000000%, test mishit error:8.000000%
	iter: 1620, training mishit error:1.333333%, test mishit error:10.666667%
	iter: 1665, training mishit error:2.666667%, test mishit error:5.333333%
	iter: 1710, training mishit error:2.666667%, test mishit error:6.666667%
	iter: 1755, training mishit error:1.333333%, test mishit error:9.333333%
	iter: 1800, training mishit error:1.333333%, test mishit error:8.000000%
	iter: 1845, training mishit error:2.666667%, test mishit error:6.666667%
	iter: 1890, training mishit error:6.666667%, test mishit error:5.333333%
	iter: 1935, training mishit error:1.333333%, test mishit error:6.666667%
	iter: 1980, training mishit error:1.333333%, test mishit error:6.666667%
	iter: 2025, training mishit error:0.000000%, test mishit error:4.000000%
	W1
	+0.874382 	+0.348000 	-1.204501 	+0.477997 	-0.287412 
	-0.101291 	-0.784060 	+0.275573 	-0.343450 	+0.920378 
	+0.052297 	+1.019816 	+0.757110 	+0.434185 	+1.507965 
	W2
	-0.072806 	-1.105107 	-0.042982 	-0.634214 
	+1.010558 	-0.244002 	+0.076584 	-0.295443 
	+0.394682 	+0.008587 	-1.117633 	+0.959675 
	mishit error on training data: 0.000000%
	mishit error on test data: 4.000000%
****Total learning steps when training stopped***
 	2025
