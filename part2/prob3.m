function prob3
%HW04, Problem 2 (learn f=1/x)
%Main function to setup the data and run BP.
    [ST,I] = dbstack;
    this_function = ST.name;
    [Xtrain,Dtrain,Xtest,Dtest] = generate_data();
    Xtrain_mean = mean(Xtrain(2,:));
    Xtrain_std  = std(Xtrain(2,:));
    Xtest_mean  = mean(Xtest(2,:));
    Xtest_std   = std(Xtest(2,:));
    Dtrain_mean = mean(Dtrain);
    Dtest_mean  = mean(Dtest);
    output_scale= max(Dtrain);
    Xtrain(2,:) = (Xtrain(2,:) - Xtrain_mean)/Xtrain_std;
    Xtest(2,:)  = (Xtest(2,:) - Xtest_mean)/Xtest_std;
    Dtrain      = (Dtrain - Dtrain_mean)/output_scale;
    Dtest       = (Dtest  - Dtest_mean)/output_scale;
    [mx,nx] = size(Xtrain);
    [md,nd] = size(Dtrain);
    [f,g] = make_fg(1);          %transfer function and its derivative
    h   = [5,5];                 %hidden layers (#PEs in each hidden layer)
    tol = 1e-1;                  %ok to stop learning at this error tolerance
    mu  = 1e-2;                  %learning step
    N   = 3e3;                   %maximum learning epochs
    K   = 2e1;                   %batch update size
    alpha = 0.5;                 %forgetting factor for momentum update (0<= alpha <1)
    num_layers = length(h)+1;    %hidden + output layers
    record_events = 10;          %Total number of instants at which to report statistics
    report_step_size = floor(N*nx/record_events);
    rms_train   = zeros(1,1+record_events);
    rms_test    = zeros(1,1+record_events);
    learn_steps = zeros(1,1+record_events);
    osuffix = fix(clock);        %suffix for output files
    ofile = strcat(this_function,'out',sprintf([repmat('_%d', 1, size(osuffix, 2)) '\n'],osuffix ),'.txt');
    outfile = fopen(ofile,'wt');
    outfig = strcat(this_function,'fig',sprintf([repmat('_%d', 1, size(osuffix, 2)) '\n'],osuffix ),'.fig');
    fprintf('Performance characteristics will be printed in file: %s\n',ofile);
    fprintf('Performance graphs will be plotted in file: %s\n',outfig);

    %nested function to report and record performance characteristics
    function report(iter,W)       
        training_error = rms_error(W,Xtrain,Dtrain);
        test_error     = rms_error(W,Xtest,Dtest);
        if(iter == 0)
            %Report network parameters
            fprintf(outfile,'***Initial weights***\n');
            report_weights(W);
            fprintf(outfile,'***Learning rate***\n');
            fprintf(outfile,'\t%g\n', mu);
            fprintf(outfile,'***Momentum Update (forgetting factor)***\n');
            fprintf(outfile,'\t%g\n', alpha);
            fprintf(outfile,'***Stopping Criteria = max. learn steps OR rms train and test error < tolerance***\n');
            fprintf(outfile,'\tMax learning steps\n');
            fprintf(outfile,'\t%d\n',N*nx);
            fprintf(outfile,'\tError Tolerance\n');
            fprintf(outfile,'\t%g\n',tol);
            fprintf(outfile,'***Initial RMS Error (training) ***\n');
            fprintf(outfile,'\t%f\n',training_error);
            fprintf(outfile,'***Initial RMS Error (test) ***\n');
            fprintf(outfile,'\t%f\n',test_error);
            fprintf(outfile,'***Performance indicators while running BP algorithm***\n');
        else
            fprintf(outfile,'\titer: %d, training rms error:%f, test rms error:%f\n',iter,training_error,test_error);
            fprintf('\titer: %d, training rms error:%f, test rms error:%f\n',iter,training_error,test_error);
        end
        learn_steps(1 + iter/report_step_size) = iter;
        rms_train(1 + iter/report_step_size) = training_error;
        rms_test(1 + iter/report_step_size)  = test_error;
    end

    function report_weights(W)
         fprintf(outfile,'\tW%d\n',1);
         fprintf(outfile,[repmat('\t%+8.6f ',1,mx) '\n'], W{1}); 
         for layer = 2:numel(W)
            fprintf(outfile,'\tW%d\n',layer);
            fprintf(outfile,[repmat('\t%+8.6f ',1,h(layer-1)+1) '\n'], W{layer});             
         end
    end
    
    %nested function to evaluate rms error given weights W, input X
    %and desired output D. Assume desired output is scaled.
    function error = rms_error(W,X,D)
        error = rms(output_scale*evaluate_output(W,X) - output_scale*D);
    end

    %nested function for bp error handle
    function stop = bp_stop(W)
        if(rms_error(W,Xtrain,Dtrain) < tol && rms_error(W,Xtest,Dtest)< tol) 
            stop = true;
        else
            stop = false;
        end
    end

    %nested function to evaluate ouput given weights W, input X
    function Y = evaluate_output(W,X)
        %input layer
        Y = f(W{1}*X);
        [row_x,num_x] = size(X);
        %hidden and output layers
        for i = 2:num_layers
            Y = f(W{i}*[ones(1,num_x);Y]);        
        end
    end

    %train network using belief propagation
    display('Starting training...');
    [W_trained,total_iter]  = bp(Xtrain,Dtrain,h,f,g,@bp_stop,mu,N,K,alpha,@report,report_step_size);
    
    %evaluate outputs and erros after training
    Y_trained = evaluate_output(W_trained,Xtrain);
    Y_test    = evaluate_output(W_trained,Xtest);    
    rms_train  = rms_train(1:floor(total_iter/report_step_size));
    rms_test   = rms_test(1:floor(total_iter/report_step_size));
    learn_steps = learn_steps(1:floor(total_iter/report_step_size));
    
    fprintf(outfile,'***After Training***\n');
    report_weights(W_trained);
    fprintf(outfile,'\trms error on training data: %f\n',rms_train(end));
    fprintf(outfile,'\trms error on test data: %f\n',rms_test(end));
    fprintf(outfile,'****Total learning steps when training stopped***\n \t%d\n',total_iter);
    
    %After training, plot desired outputs vs network outputs on training and test data
    Xtrain(2,:) = Xtrain_std*Xtrain(2,:) + Xtrain_mean;
    Xtest(2,:)  = Xtest_std*Xtest(2,:) + Xtest_mean;
    Dtrain      = output_scale*Dtrain + Dtrain_mean;
    Y_trained   = output_scale*Y_trained + Dtrain_mean;
    Dtest       = output_scale*Dtest  + Dtest_mean;
    Y_test      = output_scale*Y_test + Dtest_mean;
    X = [Xtrain(2,:), Xtest(2,:)];
    maxX = max(X);
    minX = min(X);
    minY = min([Dtrain,Y_trained,Dtest,Y_test]);
    maxY = max([Dtrain,Y_trained,Dtest,Y_test]);
    
    figure
    subplot(2,1,1);   %plot Input, Trained Output, Test Output
    plot(Xtrain(2,:),Dtrain,'g--*',Xtest(2,:),Dtest,'g--s',Xtrain(2,:),Y_trained,'b-*',Xtest(2,:),Y_test,'rs');
    axis([minX maxX minY maxY])
    title('After Training compare input data and actual output from neural network');
    xlabel('x');
    ylabel('1/x');
    legend('Desired Training','Desired Test','Actual Train Output','Actual Test Output')
    grid on

    subplot(2,1,2);  %plot the rms error vs learning step during training
    plot(learn_steps,rms_train,'b-*',learn_steps,rms_test,'r-s');
    title('RMS error vs Learning Step');
    xlabel('Learning Step');
    ylabel('RMS error');
    legend('Training rms error','Test rms error');
    saveas(gcf,outfig);
    fclose('all');
    display('Finished');
end


function [W,iter] = bp(X,D,h,f,g,stop_predicate,mu,N,K,alpha,report,report_step)
%Run batch update belief propagation algorithm
%input
%   X : Input training data matrix. Columns of this matrix correspond to 
%       input vectors. Bias terms should be included in X (all these are 1)
%   D : Ouput training data matrix. Columns of this matrix correspond to
%       desired outputs for the respective inputs.
%   h : vector representing network topology. ith element of this vector
%       denotes number of processing elements in ith hidden layer
%   f : transfer function of the processing elements. This must be a 
%       function handle with one input parameter
%   g : derivative of the transfer function 'f'. This also must be a
%       function handle with one input parameter.
%   stop_predicate: predicate (function handle) to indicate if BP stops for 
%                   current set of weights 'W' before max iterations. 
%                   The max iterations are determined using 'N' (see below)
%                   and number of training patters, that is matrix X(see above).
%   mu: learning rate for BP algorithm.
% alpha:Forgetting factor for momentum update (between 0 and 1)
%   N : maximum learning epochs for the algorithm to terminate
%       epoch = number of training samples, size of matrix X (see above).
%   K : batch update step size
%   report : callback function for reporting performace characteristics.
%            It must be a function handle with two parameters, 'iter' which
%            is current iteration and 'W' which is current set of weights.
%   report_step : call the 'report' callback every 'report_step'
%                iteratioins.
%output
%   W : A cell array represnting the weights of the network in each layer.
%       W{i} contains the weigth matrix for ith layer. 
    [mx,nx] = size(X);
    [md,nd] = size(D);
    num_layers = length(h)+1;   % hidden layers + output layer
      
    if (nx ~= nd)
        display('ERROR: bp : number of training inputs and outputs not same');
        return;
    end
      
    % Reserve memory for weights 'W', activation levels 'Net', outputs Y,
    % Delta (errros) at each layer.
    Y     = cell(1,num_layers);
    Net   = cell(1,num_layers);
    W     = cell(1,num_layers);
    G     = cell(1,num_layers);
    Delta = cell(1,num_layers);
    deltaW= cell(1,num_layers);
    previous_deltaW = cell(1,num_layers);
       
    for layer = 1:length(h)
        Y{layer}   = zeros(h(layer),1);
        Net{layer} = zeros(h(layer),1);
        G{layer}   = zeros(h(layer),h(layer));
    end
    Y{num_layers}     = zeros(md,1);
    Net{num_layers}   = zeros(md,1);
    G{num_layers}     = zeros(md,md);
    Delta{num_layers} = zeros(md,1);

    %randomize initial weights
%     W{1} = sqrt(1/mx)*2*(rand(h(1),mx,'double') - 0.5);
    W{1} = 0.2*(rand(h(1),mx,'double') - 0.5);
    deltaW{1} = zeros(h(1),mx);
     previous_deltaW{1} = zeros(h(1),mx);
    for layer = 2:length(h)
%         W{layer} = sqrt(1/(h(layer-1)+1))*2*(rand(h(layer),h(layer-1)+1,'double') - 0.5);
       W{layer} = 0.2*(rand(h(layer),h(layer-1)+1,'double') - 0.5);
       deltaW{layer} = zeros(h(layer),h(layer-1)+1);
       previous_deltaW{layer} = zeros(h(layer),h(layer-1)+1);
    end
%    W{num_layers} = sqrt(1/(h(end)+1))*2*(rand(md,h(end)+1,'double') - 0.5);
    W{num_layers} = 0.2*(rand(md,h(end)+1,'double') - 0.5);
    deltaW{num_layers} = zeros(md,h(end)+1);
    previous_deltaW{num_layers} = zeros(md,h(end)+1);

    
    for layer = length(h):-1:1
        [rG,cG] = size(G{layer});
        [rW,cW] = size(W{layer+1});
        [rD,cD] = size(Delta{layer+1});
        Delta{layer} = zeros(rG,cD);
    end

    %nested function to do feedforward for one learning step
    function feedforward(k)
        Net{1} = W{1}*X(:,k);                 %input layer
        Y{1}   = f(Net{1});
        
        for i = 2:num_layers                  %hidden and output layers
            Net{i} = W{i}*[1;Y{i-1}];
            Y{i} = f(Net{i});        
        end            
    end
    
    %nested function to feedback errors for one learning step
    function feedback(k)
        G{num_layers}(1:(md+1):end)= g(Net{num_layers});          %output layer
        Delta{num_layers} = G{num_layers}*(D(:,k)-Y{num_layers});
        
        for i = num_layers-1:-1:1                                 %hidden layers
            G{i}(1:h(i)+1:end) = g(Net{i});
            Delta{i} = G{i}*W{i+1}(:,2:end)'*Delta{i+1};
        end
    end
    
    %nested function to evaluate change in weights for all the layers
    function update_delta_weights(k)
        deltaW{1} = deltaW{1} + mu*Delta{1}*X(:,k)';   %input layer
        
        for i = 2:num_layers                           %remaining layers
            deltaW{i} = deltaW{i} + mu*Delta{i}*[1;Y{i-1}]';      
        end            
    end

    %nested function to reset weight deltas to zero
    function reset_delta_weights
        for i=1:num_layers
            deltaW{i} = deltaW{i}*0;
        end
    end

    %update weights : that is, do 'W + deltaW' (includes momentum update)
    function update_weights
        for i = 1:num_layers
            W{i} = W{i} + deltaW{i} + alpha* previous_deltaW{i};
            previous_deltaW{i} = deltaW{i};
        end
    end
    
    iter = 0;             %total iterations counter
    batch_iter = 0;       %counter for iteration  within a batch
    report(iter,W);       %make initial report before starting BP
    
    % Backpropagation algorithm for training the network
    for epoch = 1:N
        for p = randperm(nx)     
            feedforward(p);              %feedforward
            feedback(p);                 %feedback errors
            update_delta_weights(p);     %evaluate change in weights (weight deltas)
            batch_iter = batch_iter + 1; %increment iteration counters
            iter = iter+1;
            
            if(mod(batch_iter,K) == 0)   %update weights after batch size = 'K' iterations
                update_weights();
                reset_delta_weights();
                batch_iter = 0;
            end
            
            if(mod(iter,report_step) == 0)
                report(iter,W);         %perforamce record function callback
            end            
        end
        
        if(stop_predicate(W))           %stop learning if stopping criteria true    
            break;
        end
    end
end

function [Xtrain,Dtrain,Xtest,Dtest] = generate_data
%Generate training and test data for f(x) = 1/x
%input
%   no input
%output
%   [Xtrain,Dtrain] = input and output for training
%   [Xtest,Dtest]   = input and output for testing
    npoints=300;
    x = rand(1,npoints)/0.9 + 0.1;
    y = 1./x;
    
    Xtrain = x(1:200);
    Dtrain = y(1:200);
    [Xtrain,sind] = sort(Xtrain);
    Dtrain = Dtrain(sind);
    Xtrain = [ones(1,200);Xtrain];
    
    Xtest  = x(201:300);
    Dtest  = y(201:300);   
    [Xtest,sind] = sort(Xtest);
    Dtest = Dtest(sind);
    Xtest  = [ones(1,100);Xtest];
end

function [f,g] = make_fg (a)
%Generate transfer function and its derivative
%input
%   a : slope of the transfer function
%output
%   f : hyperbolic tangent with slope 'a'
%   g : derivative of function 'f'
    f = @(x)(tanh(a*x));
    g = @(x)( a*(1-f(x).^2));
end 