function hw04main
%HW04, main function
    prob3();   %run experiments and collect results for problem3
    prob4();   %run experiments and collect results for problem4
end

function prob3
%HW04, Problem 3 (learn f=1/x)
%Function to setup the experiments and run BP.
    [ST,I] = dbstack;
    this_function = ST.name;
    [Xtrain,Dtrain,Xtest,Dtest] = generate_data_prob3();
    Xtrain_mean = mean(Xtrain(2,:));
    Xtrain_std  = std(Xtrain(2,:));
    Xtest_mean  = mean(Xtest(2,:));
    Xtest_std   = std(Xtest(2,:));
    Dtrain_mean = mean(Dtrain);
    Dtest_mean  = mean(Dtest);
    output_scale= max(Dtrain);
    Xtrain(2,:) = (Xtrain(2,:) - Xtrain_mean)/Xtrain_std;
    Xtest(2,:)  = (Xtest(2,:) - Xtest_mean)/Xtest_std;
    Dtrain      = (Dtrain - Dtrain_mean)/output_scale;
    Dtest       = (Dtest  - Dtest_mean)/output_scale;
    [mx,nx] = size(Xtrain);
    [md,nd] = size(Dtrain);
    a     = 1.7159;              % transfer function = a*tanh(bx)
    b     = 2/3;
    [f,g] = make_fg(a,b);        %transfer function and its derivative
    h   = [5,5];                 %hidden layers (#PEs in each hidden layer)
    tol = 1e-1;                  %ok to stop learning at this error tolerance
    mu  = 1e-2;                  %learning step
    N   = 3e3;                   %maximum learning epochs
    K   = 2e1;                   %batch update size
    alpha = 0.5;                 %forgetting factor for momentum update (0<= alpha <1)
    num_layers = length(h)+1;    %hidden + output layers
    record_events = 100;          %Total number of instants at which to report statistics
    recall_step_size = floor(N*nx/record_events);
    rms_train   = zeros(1,1+record_events);
    rms_test    = zeros(1,1+record_events);
    learn_steps = zeros(1,1+record_events);
    osuffix = fix(clock);        %suffix for output files
    ofile = strcat(this_function,'out',sprintf([repmat('_%d', 1, size(osuffix, 2)) '\n'],osuffix ),'.txt');
    outfile = fopen(ofile,'wt');
    outfig = sprintf([repmat('_%d', 1, size(osuffix, 2))],osuffix );
    ofig = strcat(this_function,'fig',sprintf([repmat('_%d', 1, size(osuffix, 2)) '\n'],osuffix ),'.fig');
    fprintf('Performance characteristics will be printed in file: %s\n',ofile);
    fprintf('Performance graphs will be plotted in file: %s\n',ofig);

    %nested function to recall and record performance characteristics
    function bp_recall(iter,W)       
        training_error = rms_error(W,Xtrain,Dtrain);
        test_error     = rms_error(W,Xtest,Dtest);
        if(iter == 0)
            %Report network parameters
            fprintf(outfile,'----------BP LEARN STARTED----------\n');
            fprintf(outfile,'***Initial weights***\n');
            report_weights(W);
            fprintf(outfile,'***Learning rate***\n');
            fprintf(outfile,'\t%g\n', mu);
            fprintf(outfile,'***Momentum Update (forgetting factor)***\n');
            fprintf(outfile,'\t%g\n', alpha);
            fprintf(outfile,'***Stopping Criteria = max. learn steps OR rms train and test error < tolerance***\n');
            fprintf(outfile,'\tMax learning steps\n');
            fprintf(outfile,'\t%d\n',N*nx);
            fprintf(outfile,'\tError Tolerance\n');
            fprintf(outfile,'\t%g\n',tol);
            fprintf(outfile,'***Initial RMS Error (training) ***\n');
            fprintf(outfile,'\t%f\n',training_error);
            fprintf(outfile,'***Initial RMS Error (test) ***\n');
            fprintf(outfile,'\t%f\n',test_error);
            fprintf(outfile,'***Performance indicators while running BP algorithm***\n');
        else
            fprintf(outfile,'\titer: %d, training rms error:%f, test rms error:%f\n',iter,training_error,test_error);
            fprintf('\titer: %d, training rms error:%f, test rms error:%f\n',iter,training_error,test_error);
        end
        learn_steps(1 + iter/recall_step_size) = iter;
        rms_train(1 + iter/recall_step_size) = training_error;
        rms_test(1 + iter/recall_step_size)  = test_error;
    end

    function report_weights(W)
         fprintf(outfile,'\tW%d\n',1);
         fprintf(outfile,[repmat('\t%+8.6f ',1,mx) '\n'], W{1}); 
         for layer = 2:numel(W)
            fprintf(outfile,'\tW%d\n',layer);
            fprintf(outfile,[repmat('\t%+8.6f ',1,h(layer-1)+1) '\n'], W{layer});             
         end
    end
    
    %nested function to evaluate rms error given weights W, input X
    %and desired output D. Assume desired output is scaled.
    function error = rms_error(W,X,D)
        error = rms(output_scale*evaluate_output(W,X) - output_scale*D);
    end

    %nested function for bp error handle
    function stop = bp_stop(W)
        if(rms_error(W,Xtrain,Dtrain) < tol && rms_error(W,Xtest,Dtest)< tol) 
            stop = true;
        else
            stop = false;
        end
    end

    %nested function to evaluate ouput given weights W, input X
    function Y = evaluate_output(W,X)
        %input layer
        Y = f(W{1}*X);
        [row_x,num_x] = size(X);
        %hidden and output layers
        for i = 2:num_layers
            Y = f(W{i}*[ones(1,num_x);Y]);        
        end
    end
    
    xtrain(2,:) = Xtrain_std*Xtrain(2,:) + Xtrain_mean;
    xtest(2,:)  = Xtest_std*Xtest(2,:) + Xtest_mean;
    dtrain      = output_scale*Dtrain + Dtrain_mean;
    dtest       = output_scale*Dtest  + Dtest_mean;
    Ytrain      = dtrain*0;
    Ytest       = dtest*0;
    axisXY      = [0.1 1 1 10];
    col_buffer  = {'blue' , 'red', 'black',  'magenta', 'yellow', 'cyan'};
    
    function init_plot(fig)
        figure(fig)
        subplot(2,1,1);   %plot Input, Trained Output, Test Output
        hold on;
        plot(xtrain(2,:),dtrain,'g--*','LineWidth',2,'markers',2);
        axis(axisXY);
        title('Training data : Desired Vs Actual output');
        xlabel('x'); ylabel('1/x');
        leg1{end+1} = 'Desired';
        legend(leg1);
        grid on;
        hold off;
        
        subplot(2,1,2);
        hold on
        plot(xtest(2,:),dtest,'g--s','LineWidth',2,'markers',2);
        axis(axisXY);
        title('Test data : Desired Vs Actual output');
        xlabel('x'); ylabel('1/x');
        leg2{end+1} = 'Desired';
        legend(leg2);
        grid on;
        hold off;
        
        figure(fig+1)
        subplot(2,1,1);
        hold on
        title('Learning History : Training');
        xlabel('Learning Step');
        ylabel('RMS error');
        axis([1e3 6e5 0 2])
        hold off;
        
        subplot(2,1,2);
        hold on;
        title('Learning History : Test');
        xlabel('Learning Step');
        ylabel('RMS error');
        axis([1e3 6e5 0 2])
        hold off
        
    end
    
    fig=1;
    leg1 = {}; leg2 = {}; leg3 = {}; leg4 = {};
    cindex      = 1;
    init_plot(fig);

    for mu = [1e-2 5e-2 5e-3 5e-4]
        %train network using belief propagation
        fprintf('Starting training for mu = %g...\n',mu);
        [W_trained,total_iter]  = bp_learn(Xtrain,Dtrain,h,f,g,@bp_stop,mu,N,K,alpha,@bp_recall,recall_step_size);
    
        %evaluate outputs and erros after training
        Ytrain = output_scale*evaluate_output(W_trained,Xtrain) + Dtrain_mean;
        Ytest    = output_scale*evaluate_output(W_trained,Xtest) + Dtest_mean;    
        rms_train  = rms_train(1:floor(total_iter/recall_step_size));
        rms_test   = rms_test(1:floor(total_iter/recall_step_size));
        learn_steps = learn_steps(1:floor(total_iter/recall_step_size));
        
        heading    = sprintf('***After training with mu = %g***\n',mu);
        leg_suffix = sprintf('mu = %g',mu);
        print_plot_results(heading,col_buffer{cindex},fig,'learn_rate');
        cindex     = cindex+1;
    end
    
    fig = fig+2;
    leg1 = {}; leg2 = {}; leg3 = {}; leg4 = {};
    cindex = 1;
    mu = 1e-2;
    init_plot(fig);
    
     for K = [1 1e1 2e1 2e2]
        %train network using belief propagation
        fprintf('Starting training for batch size,K = %g...\n',K);
        [W_trained,total_iter]  = bp_learn(Xtrain,Dtrain,h,f,g,@bp_stop,mu,N,K,alpha,@bp_recall,recall_step_size);
    
        %evaluate outputs and erros after training
        Ytrain = output_scale*evaluate_output(W_trained,Xtrain) + Dtrain_mean;
        Ytest    = output_scale*evaluate_output(W_trained,Xtest) + Dtest_mean;    
        rms_train  = rms_train(1:floor(total_iter/recall_step_size));
        rms_test   = rms_test(1:floor(total_iter/recall_step_size));
        learn_steps = learn_steps(1:floor(total_iter/recall_step_size));
        
        heading    = sprintf('***After training with batch size, K = %g***\n',K);
        leg_suffix = sprintf('K = %g',K);
        print_plot_results(heading,col_buffer{cindex},fig,'batch_size');
        cindex     = cindex+1;
     end
     
    fig  = fig+2;
    leg1 = {}; leg2 = {}; leg3 = {}; leg4 = {};
    cindex = 1;
    K = 2e1;
    init_plot(fig);
    
    for alpha = [0.1 0.25 0.5 0.75]
        %train network using belief propagation
        fprintf('Starting training for momentum, alpha = %g...\n',K);
        [W_trained,total_iter]  = bp_learn(Xtrain,Dtrain,h,f,g,@bp_stop,mu,N,K,alpha,@bp_recall,recall_step_size);
    
        %evaluate outputs and erros after training
        Ytrain = output_scale*evaluate_output(W_trained,Xtrain) + Dtrain_mean;
        Ytest    = output_scale*evaluate_output(W_trained,Xtest) + Dtest_mean;    
        rms_train  = rms_train(1:floor(total_iter/recall_step_size));
        rms_test   = rms_test(1:floor(total_iter/recall_step_size));
        learn_steps = learn_steps(1:floor(total_iter/recall_step_size));
        
        heading    = sprintf('***After training with momentum, alpha = %g***\n',K);
        leg_suffix = sprintf('alpha = %g',alpha);
        print_plot_results(heading,col_buffer{cindex},fig,'momentum_alpha');
        cindex     = cindex+1;
    end
    
    fig  = fig+2;
    leg1 = {}; leg2 = {}; leg3 = {}; leg4 = {};
    cindex = 1;
    alpha = 0.5;
    init_plot(fig);
    
    ch = {[5] , [10], [15], [5 5]};
    for hi = 1:numel(ch)
        h = ch{hi};
        lenh = length(h);
        num_layers = length(h)+1;    %hidden + output layers
        %train network using belief propagation
        hstr = sprintf([ '[' repmat('%d ',1,lenh) ']'], h); 
        fprintf('Starting training for hidden layers,h = %s...\n',hstr );
        [W_trained,total_iter]  = bp_learn(Xtrain,Dtrain,h,f,g,@bp_stop,mu,N,K,alpha,@bp_recall,recall_step_size);
    
        %evaluate outputs and erros after training
        Ytrain = output_scale*evaluate_output(W_trained,Xtrain) + Dtrain_mean;
        Ytest    = output_scale*evaluate_output(W_trained,Xtest) + Dtest_mean;    
        rms_train  = rms_train(1:floor(total_iter/recall_step_size));
        rms_test   = rms_test(1:floor(total_iter/recall_step_size));
        learn_steps = learn_steps(1:floor(total_iter/recall_step_size));
        
        heading    = sprintf('***After training with hidden layers, h = %s***\n',hstr);
        leg_suffix = sprintf('h = %s',hstr);
        print_plot_results(heading,col_buffer{cindex},fig,'hidden_layers');
        cindex     = cindex+1;
    end
    
    
    %Best results
    h = [5 5];
    lenh = length(h);
    num_layers = length(h)+1;    %hidden + output layers
    mu = 0.01;
    par = {[20 0.5], [20 0.25], [1 0.25], [1 0.5],  }; %Each cell is [K, aplha]
    hstr = sprintf([ '[' repmat('%d ',1,lenh) ']'], h); 

    
    fig  = fig+2;
    leg1 = {}; leg2 = {}; leg3 = {}; leg4 = {};
    cindex = 1;
    init_plot(fig);
    
    
    for pi = 1:numel(par)
        K = par{pi}(1);
        alpha = par{pi}(2);
        %train network using belief propagation
        fprintf('Starting training for hidden layers,h = %s, mu=%g, K=%d, alpha=%g...\n',hstr, mu , K, alpha );
        [W_trained,total_iter]  = bp_learn(Xtrain,Dtrain,h,f,g,@bp_stop,mu,N,K,alpha,@bp_recall,recall_step_size);
    
        %evaluate outputs and erros after training
        Ytrain = output_scale*evaluate_output(W_trained,Xtrain) + Dtrain_mean;
        Ytest    = output_scale*evaluate_output(W_trained,Xtest) + Dtest_mean;    
        rms_train  = rms_train(1:floor(total_iter/recall_step_size));
        rms_test   = rms_test(1:floor(total_iter/recall_step_size));
        learn_steps = learn_steps(1:floor(total_iter/recall_step_size));
        
        heading    = sprintf('***After training with hidden layers, h = %s, mu=%g, K=%d, alpha=%g...\n',hstr, mu , K, alpha);
        leg_suffix = sprintf('h=%s,mu=%g,K=%d,alpha=%g',hstr, mu , K, alpha);
        print_plot_results(heading,col_buffer{cindex},fig,'best_three');
        cindex     = cindex+1;
    end
    
 
     
    function print_plot_results(heading,col,fig,figfile)
        fprintf(outfile,heading);
        report_weights(W_trained);
        fprintf(outfile,'\trms error on training data: %f\n',rms_train(end));
        fprintf(outfile,'\trms error on test data: %f\n',rms_test(end));
        fprintf(outfile,'****Total learning steps when training stopped***\n \t%d\n',total_iter);
    
        %After training, plot desired outputs vs network outputs on training and test data
        figure(fig)
        subplot(2,1,1);   %plot Input, Trained Output, Test Output
        hold on;
        plot(xtrain(2,:),Ytrain,'color',col,'markers',2,'LineWidth',2);
        leg1{end+1} = sprintf('%s',leg_suffix);
        legend(leg1);
        hold off;
        subplot(2,1,2);
        hold on;
        plot(xtest(2,:),Ytest,'color',col,'markers',2,'LineWidth',2);
        leg2{end+1} = sprintf('%s',leg_suffix);  
        legend(leg2);
        hold off;
        saveas(gcf,sprintf('%sfig_data_compare_%s_%s.fig',this_function,figfile,outfig));

        figure(fig+1)
        subplot(2,1,1);
        hold on;
        plot(learn_steps,rms_train,'color',col,'markers',2,'LineWidth',2);
        leg3{end+1} = sprintf('%s',leg_suffix); 
        legend(leg3);
        hold off;
        
        subplot(2,1,2);  
        hold on;
        plot(learn_steps,rms_test,'color',col,'markers',2,'LineWidth',2);
        leg4{end+1} = sprintf('%s',leg_suffix);  
        legend(leg4);
        hold off;
        saveas(gcf,sprintf('%sfig_error_history_%s_%s.fig',this_function,figfile,outfig));
    end

    fclose('all');
    display('Finished');
end

function prob4
%HW04, Problem 4 (classify iris dataset)
%Function to setup the experiments and run BP.
    [ST,I] = dbstack;
    this_function = ST.name;
    [Xtrain,Dtrain,Xtest,Dtest] = generate_data_prob4();
    Xtrain_mean =  mean(Xtrain(2:end,:),2);
    Xtrain_std  = std(Xtrain(2:end,:),0,2);
    Xtest_mean =  mean(Xtest(2:end,:),2);
    Xtest_std  = std(Xtest(2:end,:),0,2);
    Xtrain(2:end,:) = bsxfun(@minus, Xtrain(2:end,:),Xtrain_mean);
    Xtrain(2:end,:) = bsxfun(@rdivide, Xtrain(2:end,:),Xtrain_std);
    Xtest(2:end,:) = bsxfun(@minus, Xtest(2:end,:),Xtest_mean);
    Xtest(2:end,:) = bsxfun(@rdivide, Xtest(2:end,:),Xtest_std);
    Dtrain(Dtrain==0) = -1;
    Dtest(Dtest==0) = -1;
    

    [mx,nx] = size(Xtrain);
    [md,nd] = size(Dtrain);
    a     = 1.7159;              % transfer function = a*tanh(bx)
    b     = 2/3;
    [f,g] = make_fg(a,b);        %transfer function and its derivative
    h   = [3];                   %hidden layers (#PEs in each hidden layer)
    test_tol = 4;                %ok to stop learning at this error percentage mishits tolerance
    train_tol = 0;               %ok to stop learning at this error percentage mishits tolerance
    mu  = 1e-2;                  %learning step
    N   = 3e3;                   %maximum learning epochs
    K   = 2e1;                   %batch update size
    alpha = 0.5;                 %forgetting factor for momentum update (0<= alpha <1)
    num_layers = length(h)+1;    %hidden + output layers
    record_events = 5000;        %Total number of instants at which to report statistics
    recall_step_size = floor(N*nx/record_events);
    mishit_train   = zeros(1,1+record_events);
    mishit_test    = zeros(1,1+record_events);
    learn_steps = zeros(1,1+record_events);
    osuffix = fix(clock);        %suffix for output files
    ofile = strcat(this_function,'out',sprintf([repmat('_%d', 1, size(osuffix, 2)) '\n'],osuffix ),'.txt');
    outfile = fopen(ofile,'wt');
    outfig = sprintf([repmat('_%d', 1, size(osuffix, 2))],osuffix );
    ofig = strcat(this_function,'fig',sprintf([repmat('_%d', 1, size(osuffix, 2)) '\n'],osuffix ),'.fig');
    fprintf('Performance characteristics will be printed in file: %s\n',ofile);
    fprintf('Performance graphs will be plotted in file: %s\n',ofig);

 %nested function to recall and record performance characteristics
    function bp_recall(iter,W)       
        training_error = mishit_error(W,Xtrain,Dtrain);
        test_error     = mishit_error(W,Xtest,Dtest);
        if(iter == 0)
            %Report network parameters
            fprintf(outfile,'----------BP LEARN STARTED----------\n');
            fprintf(outfile,'***Initial weights***\n');
            report_weights(W);
            fprintf(outfile,'***Learning rate***\n');
            fprintf(outfile,'\t%g\n', mu);
            fprintf(outfile,'***Momentum Update (forgetting factor)***\n');
            fprintf(outfile,'\t%g\n', alpha);
            fprintf(outfile,'***Stopping Criteria = max. learn steps OR mishit (percentage) train and test error < tolerance***\n');
            fprintf(outfile,'\tMax learning steps\n');
            fprintf(outfile,'\t%d\n',N*nx);
            fprintf(outfile,'\tTraiing Error Tolerance\n');
            fprintf(outfile,'\t%g%%\n',train_tol);
            fprintf(outfile,'\tTest Error Tolerance\n');
            fprintf(outfile,'\t%g%%\n',test_tol);
            fprintf(outfile,'***Initial mishit Error for training ***\n');
            fprintf(outfile,'\t%f%%\n',training_error);
            fprintf(outfile,'***Initial mishit Error for test ***\n');
            fprintf(outfile,'\t%f%%\n',test_error);
            fprintf(outfile,'***Performance indicators while running BP algorithm***\n');
        else
            fprintf(outfile,'\titer: %d, training mishit error:%f%%, test mishit error:%f%%\n',iter,training_error,test_error);
            fprintf('\titer: %d, training mishit error:%f%%, test mishit error:%f%%\n',iter,training_error,test_error);
        end
        learn_steps(1 + iter/recall_step_size) = iter;
        mishit_train(1 + iter/recall_step_size) = training_error;
        mishit_test(1 + iter/recall_step_size)  = test_error;
    end

    function report_weights(W)
         fprintf(outfile,'\tW%d\n',1);
         fprintf(outfile,[repmat('\t%+8.6f ',1,mx) '\n'], W{1}); 
         for layer = 2:numel(W)
            fprintf(outfile,'\tW%d\n',layer);
            fprintf(outfile,[repmat('\t%+8.6f ',1,h(layer-1)+1) '\n'], W{layer});             
         end
    end
    
    %nested function to evaluate mishit error given weights W, input X
    %and desired output D.
    function error = mishit_error(W,X,D)
        [mD, nD] = size(D);
        error = evaluate_output(W,X) - D;
        error(error~=0) = 1;
        error = mean(error);
        error(error~=0) = 1;
        mishits = sum(error);
        error = (mishits/nD)*100;
    end

    %nested function for bp error handle
    function stop = bp_stop(W)
        if(mishit_error(W,Xtrain,Dtrain) <= train_tol && mishit_error(W,Xtest,Dtest)<= test_tol) 
            stop = true;
        else
            stop = false;
        end
    end

    %nested function to evaluate ouput given weights W, input X
    function Y = evaluate_output(W,X)
        %input layer
        Y = f(W{1}*X);
        [row_x,num_x] = size(X);
        %hidden and output layers
        for i = 2:num_layers
            Y = f(W{i}*[ones(1,num_x);Y]);        
        end
        Y(Y>0) =  1;
        Y(Y<0) = -1;
    end

   [W_trained,total_iter]  = bp_learn(Xtrain,Dtrain,h,f,g,@bp_stop,mu,N,K,alpha,@bp_recall,recall_step_size);
   %evaluate outputs and erros after training
   Ytrain = evaluate_output(W_trained,Xtrain);
   Ytest    = evaluate_output(W_trained,Xtest);    
   mishit_train  = mishit_train(1:floor(total_iter/recall_step_size));
   mishit_test   = mishit_test(1:floor(total_iter/recall_step_size));
   learn_steps = learn_steps(1:floor(total_iter/recall_step_size));
   
   report_weights(W_trained);
   fprintf(outfile,'\tmishit error on training data: %f%%\n',mishit_error(W_trained,Xtrain,Dtrain));
   fprintf(outfile,'\tmishit error on test data: %f%%\n',mishit_error(W_trained,Xtest,Dtest));
   fprintf(outfile,'****Total learning steps when training stopped***\n \t%d\n',total_iter);
   
   Ytrain(Ytrain<0) = 0;
   Ytest(Ytest<0)   = 0;
   Dtrain(Dtrain<0) = 0;
   Dtest(Dtest<0)   = 0;
   mul4plot = [1;2;3];
   Ytrain = 3*mean(bsxfun(@times, Ytrain,mul4plot));
   Ytest = 3*mean(bsxfun(@times, Ytest,mul4plot));
   Dtrain = 3*mean(bsxfun(@times, Dtrain,mul4plot));
   Dtest = 3*mean(bsxfun(@times, Dtest,mul4plot));

   
   figure;
   subplot(2,1,1);
   plot(Dtrain,'bs');
   hold on;
   plot(Ytrain,'r*');
   title('Training: Desired Vs Actual');
   xlabel('Training sample');
   ylabel('          Setosa          Versacolor          Virginica          ');
   ylim([0 4]);
   legend('Desired','Actual');
   hold off;
   
   subplot(2,1,2);
   plot(Dtest,'bs');
   hold on;
   plot(Ytest,'r*');
   ylim([0 4]);
   title('Test: Desired Vs Actual');
   xlabel('Training sample');
   ylabel('          Setosa          Versacolor          Virginica          ');
   legend('Desired','Actual');
   hold off;
   saveas(gcf,sprintf('%sfig1_%s.fig',this_function,outfig));

   figure;
   plot(learn_steps,mishit_train,'b-*','markers',2,'LineWidth',2);
   hold on
   plot(learn_steps,mishit_test,'r-s','markers',2,'LineWidth',2);
   legend('Training error', 'Test error');
   title('Learning History');
   xlabel('learning step');
   ylabel('mishit percentage error');
   grid on;
   grid minor;
   hold off
   saveas(gcf,sprintf('%sfig2_%s.fig',this_function,outfig));
   
   fclose('all');
end


function [W,iter] = bp_learn(X,D,h,f,g,stop_predicate,mu,N,K,alpha,bp_report,report_step)
%Run batch update belief propagation algorithm
%input
%   X : Input training data matrix. Columns of this matrix correspond to 
%       input vectors. Bias terms should be included in X (all these are 1)
%   D : Ouput training data matrix. Columns of this matrix correspond to
%       desired outputs for the respective inputs.
%   h : vector representing network topology. ith element of this vector
%       denotes number of processing elements in ith hidden layer
%   f : transfer function of the processing elements. This must be a 
%       function handle with one input parameter
%   g : derivative of the transfer function 'f'. This also must be a
%       function handle with one input parameter.
%   stop_predicate: predicate (function handle) to indicate if BP stops for 
%                   current set of weights 'W' before max iterations. 
%                   The max iterations are determined using 'N' (see below)
%                   and number of training patters, that is matrix X(see above).
%   mu: learning rate for BP algorithm.
% alpha:Forgetting factor for momentum update (between 0 and 1)
%   N : maximum learning epochs for the algorithm to terminate
%       epoch = number of training samples, size of matrix X (see above).
%   K : batch update step size
%   bp_report : callback function for reporting performace characteristics.
%            It must be a function handle with two parameters, 'iter' which
%            is current iteration and 'W' which is current set of weights.
%   report_step : call the 'report' callback every 'report_step'
%                iteratioins.
%output
%   W : A cell array represnting the weights of the network in each layer.
%       W{i} contains the weigth matrix for ith layer. 
    [mx,nx] = size(X);
    [md,nd] = size(D);
    num_layers = length(h)+1;   % hidden layers + output layer
      
    if (nx ~= nd)
        display('ERROR: bp : number of training inputs and outputs not same');
        return;
    end
      
    % Reserve memory for weights 'W', activation levels 'Net', outputs Y,
    % Delta (errros) at each layer.
    Y     = cell(1,num_layers);
    Net   = cell(1,num_layers);
    W     = cell(1,num_layers);
    G     = cell(1,num_layers);
    Delta = cell(1,num_layers);
    deltaW= cell(1,num_layers);
    previous_deltaW = cell(1,num_layers);
       
    for layer = 1:length(h)
        Y{layer}   = zeros(h(layer),1);
        Net{layer} = zeros(h(layer),1);
        G{layer}   = zeros(h(layer),h(layer));
    end
    Y{num_layers}     = zeros(md,1);
    Net{num_layers}   = zeros(md,1);
    G{num_layers}     = zeros(md,md);
    Delta{num_layers} = zeros(md,1);

    %randomize initial weights
%     W{1} = sqrt(1/mx)*2*(rand(h(1),mx,'double') - 0.5);
    W{1} = 0.2*(rand(h(1),mx,'double') - 0.5);
    deltaW{1} = zeros(h(1),mx);
     previous_deltaW{1} = zeros(h(1),mx);
    for layer = 2:length(h)
%       W{layer} = sqrt(1/(h(layer-1)+1))*2*(rand(h(layer),h(layer-1)+1,'double') - 0.5);
       W{layer} = 0.2*(rand(h(layer),h(layer-1)+1,'double') - 0.5);
       deltaW{layer} = zeros(h(layer),h(layer-1)+1);
       previous_deltaW{layer} = zeros(h(layer),h(layer-1)+1);
    end
%    W{num_layers} = sqrt(1/(h(end)+1))*2*(rand(md,h(end)+1,'double') - 0.5);
    W{num_layers} = 0.2*(rand(md,h(end)+1,'double') - 0.5);
    deltaW{num_layers} = zeros(md,h(end)+1);
    previous_deltaW{num_layers} = zeros(md,h(end)+1);

    
    for layer = length(h):-1:1
        [rG,cG] = size(G{layer});
        [rW,cW] = size(W{layer+1});
        [rD,cD] = size(Delta{layer+1});
        Delta{layer} = zeros(rG,cD);
    end

    %nested function to do feedforward for one learning step
    function feedforward(k)
        Net{1} = W{1}*X(:,k);                 %input layer
        Y{1}   = f(Net{1});
        
        for i = 2:num_layers                  %hidden and output layers
            Net{i} = W{i}*[1;Y{i-1}];
            Y{i} = f(Net{i});        
        end            
    end
    
    %nested function to feedback errors for one learning step
    function feedback(k)
        G{num_layers}(1:(md+1):end)= g(Net{num_layers});          %output layer
        Delta{num_layers} = G{num_layers}*(D(:,k)-Y{num_layers});
        
        for i = num_layers-1:-1:1                                 %hidden layers
            G{i}(1:h(i)+1:end) = g(Net{i});
            Delta{i} = G{i}*W{i+1}(:,2:end)'*Delta{i+1};
        end
    end
    
    %nested function to evaluate change in weights for all the layers
    function update_delta_weights(k)
        deltaW{1} = deltaW{1} + mu*Delta{1}*X(:,k)';   %input layer
        
        for i = 2:num_layers                           %remaining layers
            deltaW{i} = deltaW{i} + mu*Delta{i}*[1;Y{i-1}]';      
        end            
    end

    %nested function to reset weight deltas to zero
    function reset_delta_weights
        for i=1:num_layers
            deltaW{i} = deltaW{i}*0;
        end
    end

    %update weights : that is, do 'W + deltaW' (includes momentum update)
    function update_weights
        for i = 1:num_layers
            W{i} = W{i} + deltaW{i} + alpha* previous_deltaW{i};
            previous_deltaW{i} = deltaW{i};
        end
    end
    
    iter = 0;             %total iterations counter
    batch_iter = 0;       %counter for iteration  within a batch
    bp_report(iter,W);       %make initial report before starting BP
    
    % Backpropagation algorithm for training the network
    for epoch = 1:N
        for p = randperm(nx)     
            feedforward(p);              %feedforward
            feedback(p);                 %feedback errors
            update_delta_weights(p);     %evaluate change in weights (weight deltas)
            batch_iter = batch_iter + 1; %increment iteration counters
            iter = iter+1;
            
            if(mod(batch_iter,K) == 0)   %update weights after batch size = 'K' iterations
                update_weights();
                reset_delta_weights();
                batch_iter = 0;
            end
            
            if(mod(iter,report_step) == 0)
                bp_report(iter,W);         %perforamce record function callback
            end            
        end
        
        if(stop_predicate(W))           %stop learning if stopping criteria true    
            break;
        end
    end
end

function [Xtrain,Dtrain,Xtest,Dtest] = generate_data_prob3
%Generate training and test data for f(x) = 1/x
%input
%   no input
%output
%   [Xtrain,Dtrain] = input and output for training
%   [Xtest,Dtest]   = input and output for testing
    persistent npoints;
    persistent x;
    persistent y;
    persistent xstart;
    persistent xend;
    
    
    if(isempty(xstart) && isempty(xend) && isempty(npoints) && isempty(x) && isempty(y))
        display('generating sample points')
        xstart = 0.1 + 0.01;
        xend   = 1.0 - xstart - 0.01;
        npoints=300;
        x = rand(1,npoints)*xend + xstart;
        y = 1./x;
    end
    
    persistent xtrain;
    persistent dtrain;
    persistent xtest;
    persistent dtest;

    if(isempty(xtrain) && isempty(dtrain))
        display('generating training data')
        xtrain = x(1:200);
        dtrain = y(1:200);
        [xtrain,sind] = sort(xtrain);
        dtrain = dtrain(sind);
        xtrain = [ones(1,200);xtrain];
    end
    
    if(isempty(xtest) && isempty(dtest))
        display('generating test data')
        xtest  = x(201:300);
        dtest  = y(201:300);   
        [xtest,sind] = sort(xtest);
        dtest = dtest(sind);
        xtest  = [ones(1,100);xtest];
    end
    
    Xtrain = xtrain;
    Xtest  = xtest;
    Dtrain = dtrain;
    Dtest  = dtest;
end

function [Xtrain, Dtrain, Xtest, Dtest] = generate_data_prob4
%Generate training and test data for iris data classification
%input
%   no input (however, the excel files must be present in the same folder)
%   I first parsed the instructor provided ascii files using excel.
%output
%   [Xtrain,Dtrain] = input and output for training
%   [Xtest,Dtest]   = input and output for testing
trainD = xlsread('iris-train.xls');
testD  = xlsread('iris-test.xls');
Xtrain = trainD(:,1:4)';
Dtrain = trainD(:,5:7)';
Xtest  = testD(:,1:4)';
Dtest  = testD(:,5:7)';
[mtrain, ntrain] = size(Xtrain);
[mtest, ntest] = size(Xtest);
Xtrain = [ones(1,ntrain); Xtrain];
Xtest  = [ones(1,ntest); Xtest];
end

function [f,g] = make_fg (a,b)
%Generate transfer function and its derivative
%input
%   a : slope of the transfer function
%output
%   f : hyperbolic tangent with slope 'a'
%   g : derivative of function 'f'
    f = @(x)(a*tanh(b*x));
    g = @(x)( a*b*(1-tanh(b*x).^2));
end 