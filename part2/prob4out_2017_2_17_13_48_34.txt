----------BP LEARN STARTED----------
***Initial weights***
	W1
	+0.067073 	+0.010493 	+0.089298 	-0.026033 	+0.050308 
	+0.060675 	+0.059113 	+0.027221 	+0.059270 	-0.097827 
	-0.007528 	+0.054125 	+0.028918 	+0.060967 	+0.041598 
	W2
	-0.096533 	-0.052424 	+0.000635 	+0.040361 
	+0.033564 	-0.018083 	+0.077103 	+0.099280 
	-0.014382 	-0.043705 	+0.007855 	-0.067888 
***Learning rate***
	0.01
***Momentum Update (forgetting factor)***
	0.5
***Stopping Criteria = max. learn steps OR mishit (percentage) train and test error < tolerance***
	Max learning steps
	225000
	Traiing Error Tolerance
	0%
	Test Error Tolerance
	4%
***Initial mishit Error for training ***
	98.666667%
***Initial mishit Error for test ***
	100.000000%
***Performance indicators while running BP algorithm***
	iter: 45, training mishit error:100.000000%, test mishit error:100.000000%
	iter: 90, training mishit error:84.000000%, test mishit error:90.666667%
	iter: 135, training mishit error:36.000000%, test mishit error:34.666667%
	iter: 180, training mishit error:33.333333%, test mishit error:33.333333%
	iter: 225, training mishit error:54.666667%, test mishit error:52.000000%
	iter: 270, training mishit error:57.333333%, test mishit error:61.333333%
	iter: 315, training mishit error:33.333333%, test mishit error:34.666667%
	iter: 360, training mishit error:33.333333%, test mishit error:34.666667%
	iter: 405, training mishit error:33.333333%, test mishit error:33.333333%
	iter: 450, training mishit error:34.666667%, test mishit error:34.666667%
	iter: 495, training mishit error:33.333333%, test mishit error:34.666667%
	iter: 540, training mishit error:34.666667%, test mishit error:36.000000%
	iter: 585, training mishit error:34.666667%, test mishit error:29.333333%
	iter: 630, training mishit error:26.666667%, test mishit error:30.666667%
	iter: 675, training mishit error:25.333333%, test mishit error:25.333333%
	iter: 720, training mishit error:25.333333%, test mishit error:25.333333%
	iter: 765, training mishit error:24.000000%, test mishit error:20.000000%
	iter: 810, training mishit error:26.666667%, test mishit error:26.666667%
	iter: 855, training mishit error:22.666667%, test mishit error:20.000000%
	iter: 900, training mishit error:22.666667%, test mishit error:24.000000%
	iter: 945, training mishit error:22.666667%, test mishit error:21.333333%
	iter: 990, training mishit error:24.000000%, test mishit error:24.000000%
	iter: 1035, training mishit error:24.000000%, test mishit error:22.666667%
	iter: 1080, training mishit error:24.000000%, test mishit error:21.333333%
	iter: 1125, training mishit error:20.000000%, test mishit error:17.333333%
	iter: 1170, training mishit error:18.666667%, test mishit error:21.333333%
	iter: 1215, training mishit error:22.666667%, test mishit error:21.333333%
	iter: 1260, training mishit error:17.333333%, test mishit error:16.000000%
	iter: 1305, training mishit error:17.333333%, test mishit error:16.000000%
	iter: 1350, training mishit error:16.000000%, test mishit error:16.000000%
	iter: 1395, training mishit error:14.666667%, test mishit error:14.666667%
	iter: 1440, training mishit error:12.000000%, test mishit error:12.000000%
	iter: 1485, training mishit error:5.333333%, test mishit error:9.333333%
	iter: 1530, training mishit error:5.333333%, test mishit error:10.666667%
	iter: 1575, training mishit error:4.000000%, test mishit error:9.333333%
	iter: 1620, training mishit error:2.666667%, test mishit error:8.000000%
	iter: 1665, training mishit error:2.666667%, test mishit error:8.000000%
	iter: 1710, training mishit error:2.666667%, test mishit error:8.000000%
	iter: 1755, training mishit error:1.333333%, test mishit error:8.000000%
	iter: 1800, training mishit error:6.666667%, test mishit error:8.000000%
	iter: 1845, training mishit error:1.333333%, test mishit error:8.000000%
	iter: 1890, training mishit error:1.333333%, test mishit error:8.000000%
	iter: 1935, training mishit error:4.000000%, test mishit error:4.000000%
	iter: 1980, training mishit error:1.333333%, test mishit error:6.666667%
	iter: 2025, training mishit error:2.666667%, test mishit error:4.000000%
	iter: 2070, training mishit error:1.333333%, test mishit error:6.666667%
	iter: 2115, training mishit error:4.000000%, test mishit error:4.000000%
	iter: 2160, training mishit error:1.333333%, test mishit error:4.000000%
	iter: 2205, training mishit error:2.666667%, test mishit error:2.666667%
	iter: 2250, training mishit error:1.333333%, test mishit error:6.666667%
	iter: 2295, training mishit error:1.333333%, test mishit error:4.000000%
	iter: 2340, training mishit error:1.333333%, test mishit error:5.333333%
	iter: 2385, training mishit error:2.666667%, test mishit error:2.666667%
	W1
	-0.846556 	-0.439535 	-1.314425 	-0.339480 	+0.305264 
	-0.125268 	+0.644928 	-0.172113 	-0.365163 	-1.070844 
	+0.014161 	+1.037425 	-0.783040 	-0.229695 	+1.684219 
	W2
	-0.002065 	-1.135370 	-0.077899 	+0.697871 
	-1.065609 	+0.259553 	-0.048765 	+0.253602 
	-0.389955 	+0.088330 	-1.154882 	+0.954087 
	mishit error on training data: 0.000000%
	mishit error on test data: 4.000000%
****Total learning steps when training stopped***
 	2400
