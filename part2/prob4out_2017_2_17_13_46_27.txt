----------BP LEARN STARTED----------
***Initial weights***
	W1
	-0.019327 	-0.088216 	+0.066446 	-0.049936 	-0.030723 
	-0.044351 	+0.095431 	+0.019857 	+0.040864 	-0.054284 
	-0.076482 	+0.090862 	+0.010273 	+0.068686 	-0.090727 
	W2
	+0.053230 	-0.052645 	+0.091348 	+0.037601 
	-0.001497 	+0.091402 	-0.052133 	-0.082246 
	-0.083232 	+0.075805 	-0.049655 	+0.075135 
***Learning rate***
	0.01
***Momentum Update (forgetting factor)***
	0.5
***Stopping Criteria = max. learn steps OR mishit (percentage) train and test error < tolerance***
	Max learning steps
	225000
	Traiing Error Tolerance
	0%
	Test Error Tolerance
	4%
***Initial mishit Error for training ***
	100.000000%
***Initial mishit Error for test ***
	100.000000%
***Performance indicators while running BP algorithm***
	iter: 45, training mishit error:100.000000%, test mishit error:100.000000%
	iter: 90, training mishit error:86.666667%, test mishit error:92.000000%
	iter: 135, training mishit error:38.666667%, test mishit error:37.333333%
	iter: 180, training mishit error:37.333333%, test mishit error:33.333333%
	iter: 225, training mishit error:34.666667%, test mishit error:34.666667%
	iter: 270, training mishit error:52.000000%, test mishit error:49.333333%
	iter: 315, training mishit error:34.666667%, test mishit error:34.666667%
	iter: 360, training mishit error:34.666667%, test mishit error:33.333333%
	iter: 405, training mishit error:40.000000%, test mishit error:38.666667%
	iter: 450, training mishit error:33.333333%, test mishit error:33.333333%
	iter: 495, training mishit error:36.000000%, test mishit error:30.666667%
	iter: 540, training mishit error:28.000000%, test mishit error:28.000000%
	iter: 585, training mishit error:30.666667%, test mishit error:26.666667%
	iter: 630, training mishit error:26.666667%, test mishit error:24.000000%
	iter: 675, training mishit error:26.666667%, test mishit error:24.000000%
	iter: 720, training mishit error:25.333333%, test mishit error:21.333333%
	iter: 765, training mishit error:24.000000%, test mishit error:21.333333%
	iter: 810, training mishit error:18.666667%, test mishit error:21.333333%
	iter: 855, training mishit error:22.666667%, test mishit error:21.333333%
	iter: 900, training mishit error:16.000000%, test mishit error:18.666667%
	iter: 945, training mishit error:17.333333%, test mishit error:17.333333%
	iter: 990, training mishit error:13.333333%, test mishit error:17.333333%
	iter: 1035, training mishit error:10.666667%, test mishit error:13.333333%
	iter: 1080, training mishit error:10.666667%, test mishit error:12.000000%
	iter: 1125, training mishit error:6.666667%, test mishit error:12.000000%
	iter: 1170, training mishit error:5.333333%, test mishit error:10.666667%
	iter: 1215, training mishit error:8.000000%, test mishit error:9.333333%
	iter: 1260, training mishit error:6.666667%, test mishit error:10.666667%
	iter: 1305, training mishit error:4.000000%, test mishit error:8.000000%
	iter: 1350, training mishit error:4.000000%, test mishit error:8.000000%
	iter: 1395, training mishit error:1.333333%, test mishit error:10.666667%
	iter: 1440, training mishit error:1.333333%, test mishit error:9.333333%
	iter: 1485, training mishit error:1.333333%, test mishit error:8.000000%
	iter: 1530, training mishit error:5.333333%, test mishit error:5.333333%
	iter: 1575, training mishit error:1.333333%, test mishit error:9.333333%
	iter: 1620, training mishit error:1.333333%, test mishit error:6.666667%
	iter: 1665, training mishit error:4.000000%, test mishit error:5.333333%
	iter: 1710, training mishit error:5.333333%, test mishit error:5.333333%
	iter: 1755, training mishit error:1.333333%, test mishit error:4.000000%
	iter: 1800, training mishit error:4.000000%, test mishit error:4.000000%
	iter: 1845, training mishit error:1.333333%, test mishit error:5.333333%
	iter: 1890, training mishit error:1.333333%, test mishit error:4.000000%
	iter: 1935, training mishit error:0.000000%, test mishit error:4.000000%
	W1
	-0.919776 	+1.162039 	+0.052681 	-0.650174 	+0.113321 
	+0.227188 	+0.814287 	+0.276134 	+0.122566 	-1.067278 
	-1.020424 	-0.149181 	-0.463521 	-1.569294 	-0.510972 
	W2
	-0.060994 	-1.221175 	+0.183525 	+0.577803 
	-1.092826 	+0.432248 	-0.092724 	+1.115935 
	-0.944663 	+0.315140 	+0.102969 	-0.392613 
	mishit error on training data: 0.000000%
	mishit error on test data: 4.000000%
****Total learning steps when training stopped***
 	1950
