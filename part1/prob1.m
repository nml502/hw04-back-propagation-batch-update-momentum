function W = prob1
%Homework4-part1 Problem1
%Run simulation for Example on pg:44-45 of Colin Fayes' text.
%The network is trained using standard Backpropagation Algorithm.

    % Input and Output for XOR network training examples
    X = [1 1 1 1; 1 1 -1 -1; 1 -1 1 -1];
    D = [-1 1 1 -1];

    % Weights for first and second layer (including biases)
    W = {zeros(2,3) zeros(1,3)};

    % Reserve memory for various parameters
    Y = {zeros(2,1) zeros(1,1)};       %Outputs at each layer
    Net = Y;                           %Activation levels at each layer
    Delta = Y;                         %Deltas in BP algorithm
    G = {spdiags([1;1],0:0,2,2) spdiags(1,0:0,1,1)};    %Gradients required in BP algorithm
    

    a     = 1;                  %tanh slope
    f     = @(x) (tanh(a*x));   %transfer function is tanh(.) with slope 1.
    gradf = @(x) (1 - f(x).^2); %derivative of transfer function.

    %setup hyper-parameters
    mu  = 1e-2;                 %learning rate for BP algorithm.
    tol = 5e-2;                 %error threshold
    N   = 1e5;                  %max learning epochs


    %randomize initial weights
    %see discussion on http://stats.stackexchange.com/questions/47590/what-are-good-initial-weights-in-a-neural-network
    %The scheme in Fayes' text does not work always. Sometimes the weights
    %are initialized to local optimum.
    W{1} = sqrt(1/3)*2*(rand(2,3,'double') - 0.5);
    W{2} = sqrt(1/3)*2*(rand(1,3,'double') - 0.5);


    %Report network parameters
    REPORT = fopen('prob1_report.txt','w');
    fprintf(REPORT,'***Initial weights***\n');
    fprintf(REPORT,'\tW1\n');
    fprintf(REPORT,[repmat('\t%+8.6f ',1,3) '\n'], W{1});
    fprintf(REPORT,'\tW2\n');
    fprintf(REPORT,[repmat('\t%+8.6f ',1,3) '\n'], W{2});

    fprintf(REPORT,'***Learning rate***\n');
    fprintf(REPORT,'\t%g\n', mu);

    fprintf(REPORT,'***Stopping Criteria***\n');
    fprintf(REPORT,'\tMax learning epochs\n');
    fprintf(REPORT,'\t%d\n',N);
    fprintf(REPORT,'\tError Tolerance\n');
    fprintf(REPORT,'\t%g\n',tol);


    %calculates rms error for given set of weights
    error = @(W1,W2)( rms( f(W2*[ones(1,4);f(W1*X)]) - D ) );
    
    total_steps = 0;
    M = 1e3;
    Rms = zeros(1,N/M);

    fprintf(REPORT,'***Performance Indicators while running BP algorithm***\n');
    fprintf(REPORT,'\t%s\t %s\t %s\t %s\t %s\n','Learn_Step', 'Input_Vector', 'Actual_Output', 'Desired_Output', 'RMS_error');
    
    % Backpropagation algorithm for training this network
    for i = 1:N
        for j = randperm(length(D))      
            %feedforward
            Net{1} = W{1}*X(:,j);
            Y{1} = f(Net{1});
            Net{2} = W{2}*[1;Y{1}];
            Y{2} = f(Net{2});
            
            %feedback errors
            G{2}(1:2:end) = gradf(Net{2});
            Delta{2} = G{2}*(D(:,j)-Y{2});
            
            G{1}(1:3:end) = gradf(Net{1});
            Delta{1} = G{1} * (W{2}(:,2:end))' * Delta{2};
            
            %Update weights
            W{1} = W{1} + mu*Delta{1}*X(:,j)';
            W{2} = W{2} + mu*Delta{2}*[1;Y{1}]';        
        end
        
        total_steps = total_steps + 4;

        %report statistics every M epochs
        if(mod(i,M) == 0)
            Out = f(W{2}*[ones(1,4);f(W{1}*X)]);
            fprintf(REPORT,['\t%10d' '\t\t[' repmat('\t%+d ',1,2) ']' '\t\t%+13.4f' '\t%14d' '\t%12.6f' '\n' ], total_steps, X(2:end,1),Out(1),D(1),error(W{1},W{2}));
            fprintf(REPORT,['\t%10s' '\t\t[' repmat('\t%+d ',1,2) ']' '\t\t%+13.4f' '\t%14d' '\n' ], ' ', X(2:end,2),Out(2),D(2));   
            fprintf(REPORT,['\t%10s' '\t\t[' repmat('\t%+d ',1,2) ']' '\t\t%+13.4f' '\t%14d' '\n' ], ' ', X(2:end,3),Out(3),D(3));   
            fprintf(REPORT,['\t%10s' '\t\t[' repmat('\t%+d ',1,2) ']' '\t\t%+13.4f' '\t%14d' '\n' ], ' ', X(2:end,4),Out(4),D(4));           
            Rms(i/M) = error(W{1},W{2});
        end
            
        if(error(W{1},W{2}) < tol)
            break;
        end
    end
    
    fprintf('rms error : %f\n',error(W{1},W{2}));
    
    %report performance of the trained network
    fprintf(REPORT,'***Network Performance after training***\n');
    fprintf(REPORT,'\tTotal learning steps\n');
    fprintf(REPORT,'\t%d\n',total_steps);
    
    %plot the rms error vs learning step
    steps = 4*M:4*M:total_steps;
    Rms = Rms(steps/(4*M));
    figure()
    plot(steps,Rms);
    title('RMS error vs Learning Step');
    xlabel('Learning Step');
    ylabel('RMS error');
    
    %Tabulate desired output vs training output after training finished
    Out = f(W{2}*[ones(1,4);f(W{1}*X)]);
    fprintf(REPORT,'\t%s\t %s\t %s\n','Input_Vector', 'Training_Output', 'Desired_Output');
    fprintf(REPORT,['\t\t[' repmat('\t%+d ',1,2) ']' '\t\t%+13.4f' '\t%15d' '\n' ], X(2:end,1),Out(1),D(1));
    fprintf(REPORT,['\t\t[' repmat('\t%+d ',1,2) ']' '\t\t%+13.4f' '\t%15d' '\n' ], X(2:end,2),Out(2),D(2));
    fprintf(REPORT,['\t\t[' repmat('\t%+d ',1,2) ']' '\t\t%+13.4f' '\t%15d' '\n' ], X(2:end,3),Out(3),D(3));
    fprintf(REPORT,['\t\t[' repmat('\t%+d ',1,2) ']' '\t\t%+13.4f' '\t%15d' '\n' ], X(2:end,4),Out(4),D(4));
 
    fclose(REPORT);
