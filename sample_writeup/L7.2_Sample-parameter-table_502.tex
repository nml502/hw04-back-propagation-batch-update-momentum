\documentclass[epsfig]{article}
\usepackage{epsfig}
\usepackage{amsmath}
\usepackage{verbatim}
\usepackage{booktabs}
\usepackage{subfig}
\usepackage{graphicx}
\usepackage[english]{babel}
\textwidth 6.7in
\oddsidemargin -0.1in
\textheight 8.50in
\topmargin -0.55in
\renewcommand{\textfraction}{0.25}
\renewcommand{\floatpagefraction}{0.7}
\pagestyle{myheadings}
\def\bpar{\vskip26pt}
\def\npar{\vskip13pt}
\def\spar{\vskip10pt}
\begin{document}
\parindent=0pt
\null
\centerline{\bf Example of reporting parameters pertaining to network training }
\begin{center}
{\it You may omit the lines with the headings ``Network parameters" etc., from the table. I am using them to show groups of items that logically belong together,
therefore listing them in this order helps easy overview of the network training circumstances.

The LaTeX source file} {\bf L7.2\_Sample-parameter-table\_502.tex}  {\it of this document is posted in Owl Space under Resources/Supplements. 
}
\end{center}
\spar
\begin{table}[htbp] 
\center
\caption{Parameters of Training BP Network to Fit $f(x) = 1/x$}
  \label{tab:NP}
  %\scalebox{0.9}{ % You can scale the size of the table by changing this number
   \scalebox{1.0}{
   \begin{tabular}{p{4cm} p{.05cm} p{8cm}}
\toprule
  \multicolumn{3}{l}{\bf Network parameters} \\
\bottomrule \noalign{\smallskip}
  Topology & & $(2 + 1_{Bias})$ --- $(2 + 1_{Bias})$ --- $1$ \\
  Transfer function & & tanh with slope of 1 \\
\toprule
  \multicolumn{3}{l}{\bf Learning parameters} \\
\bottomrule \noalign{\smallskip}
  Initial weights & & drawn from U[-0.1, 0.1] \\
  Learning rate ($\alpha$) & & 0.5, decreasing with time according to the schedule below. \\
  Momentum & & none\\
  Epoch size ($Epoch$)& &  1 \\
  Stopping criteria & &  error ($Err_{stop}$) $ \le 0.05 $ OR  learn count (t) $ > 500,000 $\\
  Error measure ($Err_{stop}$)& & $\|D-y\|$ averaged over all training samples (see formula (1) below)\\\toprule
 \multicolumn{3}{l}{\bf Input / output data, representation, scaling} \\
\bottomrule \noalign{\smallskip}
  \# training samples ($N_{tr}$)& & 200 (x values drawn randomly from U[0.1,1])\\
  \# test samples ($N_{tst}$)& & 100 (x values drawn randomly from U[0.1,1])\\
  Scaling of inputs & &  map [global min, global max] to [-1.5,1.5] \\
  Scaling of outputs & &  map [global min, global max] to [-0.9,0.9] \\
%  %\# learn steps performed & & 180,365 (error threshold reached)\\
\toprule
  \multicolumn{3}{l}{\bf Parameters and error measures for performance evaluation} \\
\bottomrule \noalign{\smallskip}
  Error of fit ($Err_{fit}$) & & Root Mean Square Error (RMSE), over all samples, at a given learn count, see formula (2)\\
  \# learn steps performed & & 180,365 (threshold for $Err_{stop}$ reached)\\
  Learning rate at end & & 0.001\\
  Monitoring frequency ($m$) & &  1,000 learning steps\\

 \bottomrule \noalign{\smallskip}
 
  \end{tabular}
   } % end scalebox
\end{table}
 

Error measures for stopping criterion and for performance evaluation may be the same or can be different. Multiple metrics can also be used. The important thing is to set an interpretable criterion (as meaningful for our problem). The network's internal error formula - which is used for the weight update - should not be altered. Rather, that error should be ``translated" to a measure that is interpretable in the user's world so that we can understand how much error we accept on the scale of the given problem before we want to stop the training. We do this by scaling back the network's outputs to the user data range and computing whatever error is meaningful, in the user range. {\it (Note: the internal sum-of-squared errors that the ANN uses for its feedback in the weight update is hard to interpret because it is, in general, on a different scale than the original data. It may also be very noisy because it ismay not be averaged over a training set or over some number of learn steps. Even if we use the same sum-of-squared errors we should scale the outputs back to the original data range.)}
\npar

Followng are a few examples of error measure description. I am assuming that the $y^k$ network outputs have been scaled back to user range and the desired output values are taken in the original range. In other words, the errors are computed in the user world. My comments within an example are in italics:
\spar

(1) $ Err_{stop} = {1\over{N_{tr}}}\sum_{k=1}^{N_{tr}} ||D^k-y^k||$\label{err-stop}, where $k$ is sample index. 
\spar
{\it (In this example the stopping error is computed using the training set, which is customary. You may use either training or test output, just be sure to say which.)}
\spar

(2) $ Err_{fit} = \sqrt{{1\over{|X|}}\sum_{k=1}^{|X|}(D^k-y^k)^2}$, computed at a specific learn count.  $k$ is sample index, $X$ is either training or testing data set.  \spar
({\it In this example the error of fit is RMSE. 
For multiple outputs, sum up the squared deviations of all output PEs as  in (2.1).})
\spar
(2.1) $ Err_{fit} = \sqrt{{1\over{|X|}}\sum_{k=1}^{|X|}\sum_{i=1}^{M} (D_i^k-y_i^k)^2}$, where $k$ is sample index, $M$ is the number of output PEs .
\spar
You may also consider computing averages over some windows of learning steps, to get a smoother learning curve. Be sure to describe exactly how you form averages.

\npar
For classification, the {\it classification accuracy} is used customarily rather than MSE type errors. 
Classification accuracy is often computed as ``correct hit rate", or \% of correctly classified samples in a training or test sample set $X$): \spar

$ Acc_X = 100*\frac{\# correct\enspace hits}{|X|}\enspace \%$, where $correct\enspace hit = \cdots$ 
\spar
{\it Be sure to specify how you define ``correct hit".}

\spar 
Note: The above $ Acc_X$ is the True Positive rate. For our exercises we should always show this (partly because it is easy to see if something is ``off" when we use such simple measure). A better classification accuracy measure --- for example --- is the Cohen $\kappa$-statistic, which considers the combined effects of true and false positives and negatives, as well as compensates for chance ``hits".  You can add such - more sophisticated - measures later.
\spar
Classification accuracy is also an example of an error measure that is invariant to scaling. Therefore it could be applied (and interpreted) within a learning fucntion internally.

\bpar
{\it Example of reporting time-decreasing learning rate:}
\spar
The learning rate ($\alpha$) decreases as follows, with $t$ as the learning step. \\
$\alpha(t) = \begin{cases} 0.05 & \text{if }\text{ $t<$ 1000} \\
						   	    0.025 & \text{if }\text{$t<$ 5000} \\
						   	    0.002 & \text{if }\text{$t<$ 10000}\\
						   	    0.001 & \text{if }\text{$t\geq$ 10000}\\ 
				   \end{cases}$\\ 
\spar
 or specify the decay by a function, like:
\spar
$\alpha(t) = \alpha(0) * exp(-t/\tau)$ where $\tau = 0.001$ is the exponential decay rate.
 
 
\end{document}